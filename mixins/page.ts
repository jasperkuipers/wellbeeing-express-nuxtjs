import { Vue } from 'vue-property-decorator'
import { Mixin } from 'vue-mixin-decorator'
// import { State, Getter } from 'vuex-class'
// import axios from 'axios'
// import { Page } from '~/types'

// const URL = 'https://wp.wellbeeing.org'

@Mixin({
  computed: {
    content(): string {
      return this.$store.getters.pageContent
    },
    excerpt(): string {
      return this.$store.getters.pageExcerpt
    },
    hasMollieForm(): string {
      return this.$store.getters.pageHasMollieForm
    },
    hasSubscriptionForm(): string {
      return this.$store.getters.pageHasSubscriptionForm
    },
    isHomepage(): boolean {
      return this.$store.getters.pageIsHomepage
    },
    masthead(): string {
      return this.$store.getters.pageMasthead
    },
    mollieFormType(): string {
      return this.$store.getters.pageMollieFormType
    },
    relatedPages(): string {
      return this.$store.getters.pageRelatedPages
    },
    subscriptionFormType(): string {
      return this.$store.getters.pageSubscriptionFormType
    },
    title(): string {
      return this.$store.getters.pageTitle
    }
  },
  head() {
    const title = this.$store.getters.pageSeo?.title ? this.$store.getters.pageSeo.title : this.$store.getters.pageTitle
    const description = this.$store.getters.pageSeo?.meta?.length >= 15 ? this.$store.getters.pageSeo.meta[0].content : `${this.$store.getters.pageExcerpt.replace(/(<([^>]+)>)/gi, '').substr(0, 150)}...`

    return {
      title,
      link: [
        {
          rel: 'canonical',
          href: `https://wellbeeing.org${this.$route.path}`
        }
      ],
      meta: [
        {
          hid: 'description',
          id: 'description',
          name: 'description',
          content: description
        },
        {
          name: 'description',
          hid: 'description',
          content: description
        },
        // open graph
        { name: 'og:title', content: title },
        { name: 'og:description', content: description },
        { name: 'og:type', content: 'website' },
        { name: 'og:url', content: 'https://wellbeeing.org' },
        { name: 'og:image', content: 'https://wellbeeing.org/icon.png' },
        // twitter card
        { name: 'twitter:card', content: 'summary' },
        { name: 'twitter:site', content: 'https://wellbeeing.org' },
        { name: 'twitter:title', content: title },
        { name: 'twitter:description', content: description },
        { name: 'twitter:image', content: 'https://wellbeeing.org/icon.png' },
        { name: 'twitter:image:alt', content: 'Wellbeeing.org foundation' }
      ]
    }
  }
})
class PageMixin extends Vue {
  // asyncData(context: any) {
  //   const { app, params, store, route } = context
  // }
}

export default PageMixin
