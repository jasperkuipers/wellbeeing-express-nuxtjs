import { NuxtConfig } from '@nuxt/types'

const nuxtConfig: NuxtConfig = {
  buildModules: [
    '@nuxt/typescript-build',
    [
      '@nuxtjs/google-analytics', {
        id: 'UA-147443652-1'
      }
    ],
    '@/modules/sitemap-route-generator'
  ],
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s',
    title: 'Wellbeeing.org foundation',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'The WellBeeing.org foundation works at improving the well-being of bees. We want to help these pollinators that have helped produce our food for centuries, by providing them with a better living environment an increase their chances for survival on the short- and long term. It is time for a transition from what they can do.'
      }
    ],
    link: [
      {
        rel: 'icon',
        href: '/favicon.ico',
        type: 'image/x-icon'
      },
      {
        rel: 'alternate',
        href: 'https://www.wellbeeing.org.nl',
        hreflang: 'x-default'
      },
      {
        rel: 'alternate',
        href: 'https://www.wellbeeing.org.nl',
        hreflang: 'en-GB'
      },
      {
        rel: 'alternate',
        href: 'https://www.wellbeeing.org.nl/nl',
        hreflang: 'nl-NL'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap',
        type: 'text/css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Cookie&display=swap',
        type: 'text/css'
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/scss/app.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/register-plugins.ts',
    '~/plugins/i18n.ts'
  ],
  typescript: {
    typeCheck: true,
    ignoreNotFoundWarnings: true
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    '@nuxtjs/svg-sprite',
    'bootstrap-vue/nuxt'
  ],
  bootstrapVue: {
    bootstrapCSS: false, // Or `css: false`
    bootstrapVueCSS: false, // Or `bvCSS: false`
    componentPlugins: ['ButtonPlugin'] // 'CollapsePlugin', 'NavbarPlugin'
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config: any, ctx: any) {
      config.node = { // needed for dotenv
        fs: 'empty'
      }
    },
    babel: {
      compact: true
    }
  },
  render: {
    http2: {
      push: true,
      pushAssets: () => {
        return [
          '<https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap>; rel=preload; as=style',
          '<https://fonts.googleapis.com/css?family=Cookie&display=swap>; rel=preload; as=style'
        ]
      }
    }
  },
  robots: {
    UserAgent: '*',
    Disallow: '/backup'
  },
  /*
   ** Router extension
   */
  router: {
    middleware: ['i18n', 'page']
  },
  serverMiddleware: [
    { path: '/api', handler: '~/server/index.ts' }
  ],
  sitemap: {
    path: '/sitemap.xml',
    hostname: 'https://wellbeeing.org',
    cacheTime: 1000 * 60 * 3600
  },
  svgSprite: {
    input: '~/assets/sprites/svg',
    output: '~/assets/images',
    defaultSprite: 'sprite'
  }
}

module.exports = nuxtConfig
