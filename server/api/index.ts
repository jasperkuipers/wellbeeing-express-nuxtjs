import { Router } from 'express'

import mollie from './mollie'
import wordpress from './wordpress'

const router = Router()

router.use(mollie)
router.use(wordpress)

export default router
