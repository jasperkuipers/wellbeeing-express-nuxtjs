
import cors from 'cors'
import { Router } from 'express'
import { createMollieClient, SequenceType, PaymentMethod } from '@mollie/api-client'
import { Payment } from '@mollie/api-client/dist/types/src/types'
import axios from 'axios'

require('dotenv').config()

const router = Router()

const apiKey = process.env.MOLLIE // process.env.MOLLIE_TEST

const URL = 'https://wellbeeing.org'
const WORDPRESS_URL = 'https://wp.wellbeeing.org'

if (apiKey) {
  const mollieClient = createMollieClient({ apiKey })

  router.post('/mollie/get-customer', async (req, res) => {
    if (!req.body.id) {
      return res.status(400).json({
        status: 'error',
        error: 'req body cannot be empty, get customer failed'
      })
    }

    const id = req.body.id

    try {
      const customer = await mollieClient.customers.get(id)

      res.json(customer)
    } catch (error) {
      res.send(error)
    }
  })

  router.post('/mollie/create-customer', async (req, res) => {
    const name = req.body.name
    const email = req.body.email

    if (!name || !email) {
      return res.status(400).json({
        status: 'error',
        error: 'req body cannot be empty, create customer failed'
      })
    }

    try {
      const customer = await mollieClient.customers.create({
        name,
        email
      })

      res.json(customer)
    } catch (error) {
      res.send(error)
    }
  })

  router.post('/mollie/create-payment', async (req, res) => {
    const id = req.body.id
    const language = req.body.language ? req.body.language : ''
    const type = req.body.type ? req.body.type : ''

    let redirectUrl

    if (type === 'course') {
      if (language === 'en') {
        redirectUrl = `${URL}/courses/thank-you`
      } else {
        redirectUrl = `${URL}/nl/cursussen/bedankt`
      }
    } else {
      redirectUrl = URL
    }

    if (!id) {
      return res.status(400).json({
        status: 'error',
        error: 'req body cannot be empty, create payment failed'
      })
    }

    try {
      const payment: Payment = await mollieClient.customers_payments.create({
        customerId: id,
        amount: {
          currency: 'EUR',
          value: '99.00'
        },
        description: language === 'en' ? 'Introduction course' : 'Introductiecursus',
        method: PaymentMethod.ideal,
        redirectUrl,
        webhookUrl: `${URL}/api/mollie/payments/webhook`
      })

      // console.log(payment)
      // console.log('payment._links.checkout.href', payment._links.checkout.href)

      res.json(payment)
    } catch (error) {
      res.send(error)
    }
  })

  router.post('/mollie/create-payment-with-mandate', async (req, res) => {
    const id = req.body.id

    if (!id) {
      return res.status(400).json({
        status: 'error',
        error: 'req body cannot be empty, create payment with mandate failed'
      })
    }

    try {
      const payment: Payment = await mollieClient.customers_payments.create({
        customerId: id,
        amount: {
          currency: 'EUR',
          value: '0.01'
        },
        description: 'First payment',
        method: PaymentMethod.ideal,
        sequenceType: SequenceType.first,
        redirectUrl: URL,
        webhookUrl: `${URL}/api/mollie/payments/webhook`
      })

      // console.log(payment)
      // console.log('payment._links.checkout.href', payment._links.checkout.href)

      res.json(payment)
    } catch (error) {
      res.send(error)
    }
  })

  router.post('/mollie/create-subscription', async (req, res) => {
    const id = req.body.id
    const amount = req.body.amount

    if (!id || !amount) {
      return res.status(400).json({
        status: 'error',
        error: 'req body cannot be empty, create subscription failed'
      })
    }

    try {
      await mollieClient.customers_subscriptions.create({
        customerId: id,
        amount: { value: amount, currency: 'EUR' },
        times: 4,
        interval: '3 months',
        description: 'Quarterly payment',
        startDate: '',
        webhookUrl: `${URL}/api/mollie/payments/webhook`
      })
        .then(({ id }) => {
          // i.e. send confirmation email
        })
        .catch((error) => {
          // do some proper error handling
          console.log(error)
        })

      res.json({ result: 'success' })
    } catch (error) {
      res.send(error)
    }
  })

  router.post('/mollie/payments/webhook', cors(), (req, res) => {
    const id = req.body.id
    const bearerToken = req.body.bearerToken

    if (!id) {
      return res.status(400).json({
        status: 'error',
        error: 'req body cannot be empty, payment webhook failed'
      })
    }

    mollieClient.payments
      .get(id)
      .then(async (payment) => {
        let subtype = 'failed'

        if (payment.isPaid()) {
          subtype = 'successful'
        }

        // console.log('WEBHOOK: payment', payment)
        // console.log('WEBHOOK: payment status', payment.status)

        let result: any
        let bearerToken: string

        // first get a JWT bearer token
        try {
          result = await axios({
            method: 'post',
            url: `${WORDPRESS_URL}/wp-json/jwt-auth/v1/token`,
            data: {
              username: process.env.USERNAME,
              password: process.env.PASSWORD
            }
          })
        } catch (error) {
          // console.log(Object.keys(error), error.message)
        }

        if (result && result.data.token) {
          bearerToken = result.data.token
        } else {
          return
        }

        // get user and send confirmation email
        try {
          result = await axios({
            method: 'post',
            url: `${WORDPRESS_URL}/wp-json/api/v1/get-customer-by-id`,
            headers: {
              Authorization: `Bearer ${bearerToken}`
            },
            data: {
              id: payment.customerId
            }
          })
        } catch (error) {
          // console.log(Object.keys(error), error.message)
        }

        if (result && result.data) {
          const data = JSON.parse(result.data)

          const amount = 99
          const description = data.language === 'en' ? 'Introduction%20course' : 'Introductiecursus'

          try {
            result = await axios({
              method: 'post',
              url: `${WORDPRESS_URL}/wp-json/api/v1/send-mail`,
              headers: {
                Authorization: `Bearer ${bearerToken}`
              },
              data: {
                name: data.name,
                email: data.email,
                language: data.language,
                payment_link: `https://bunq.me/wellbeeing?description=${description}&amount=${amount}`,
                start_date: data.startDate,
                subtype: `confirmation-${subtype}`,
                type: data.type
              }
            })
          } catch (error) {
            // console.log('error', Object.keys(error), error.message)
          }
        } else {
          return
        }

        // next update the customer's payment_status
        try {
          result = await axios({
            method: 'post',
            url: `${WORDPRESS_URL}/wp-json/api/v1/update-customer`,
            headers: {
              Authorization: `Bearer ${bearerToken}`
            },
            data: {
              customer_id: payment.customerId,
              payment_status: payment.status
            }
          })
        } catch (error) {
          // console.log(Object.keys(error), error.message)
        }

        res.send(payment.status)
      })
      .catch((error) => {
        console.log('payment error', error)

        res.send(error)
      })
  })
}

export default router
