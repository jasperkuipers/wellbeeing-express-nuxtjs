import { Router } from 'express'
import NodeCache from 'node-cache'
import axios from 'axios'
import passport from 'passport'

const LocalStrategy = require('passport-local').Strategy

require('dotenv').config()

const cache = new NodeCache()
const TTL = 100000000
const url = 'https://wp.wellbeeing.org'

interface WordpressData {
  acf?: object;
  bearerToken?: string;
  forms?: object;
  frontpageEN?: object;
  frontpageNL?: object;
  menus?: object;
  pages?: object;
  test?: string;
}

passport.use(new LocalStrategy(
  (username: any, password: string, done: any) => {
    if (username === process.env.LOGIN_USERNAME && password === process.env.LOGIN_PASSWORD) {
      return done(null, { username: process.env.LOGIN_USERNAME })
    } else {
      return done(null, false)
    }
  }
))

passport.serializeUser((user: any, done: any) => {
  done(null, user.username)
})

passport.deserializeUser((username: any, done: any) => {
  done(null, { username: username })
})

const isLoggedIn = (req: any, res: any, next: any) => {
  if (req.isAuthenticated()) {
    return next()
  }

  return res.redirect('/login')
}

const router = Router()

router.post('/login', passport.authenticate('local', {
  failureRedirect: '/login',
  successRedirect: '/api/update'
}))

router.get('/update', isLoggedIn, async (req, res) => {
  if (cache) {
    // delete cache
    cache.del('data')
    // fetch data
    const data: WordpressData = await fetchData()
    // add data to cache
    cache.set('data', data, TTL)
  }

  res.send('wordpress data updated')
})

router.get('/wordpress', async (req, res) => {
  if (!cache.get('data')) {
    // fetch data
    const data: WordpressData = await fetchData()
    // add data to cache
    cache.set('data', data, TTL)
  }

  res.json(cache.get('data'))
})

async function fetchData(): Promise<WordpressData> {
  const data: WordpressData = {}
  data.test = `test: ${Date.now()}`

  // fetch frontpage(s)
  const fetchedFrontpageEN: any = await axios.get(`${url}/wp-json/wp/v2/frontpage`) // fetch frontpage en_GB
  let fetchedFrontpageNL: any

  if (fetchedFrontpageEN && fetchedFrontpageEN.data) {
    const frontpageNL = fetchedFrontpageEN.data.polylang_translations.find((translation: any) => translation.locale !== 'en_GB')
    if (frontpageNL) {
      fetchedFrontpageNL = await axios.get(
        `${url}/wp-json/wp/v2/pages/${frontpageNL.id}` // fetch frontpage nl_NL
      )
    }
  }

  // fetch pages
  const fetchedPages: any = await axios.get(`${url}/wp-json/wp/v2/pages?per_page=100`)

  // fetch forms
  const fetchedForms: any = await axios.get(`${url}/wp-json/wp/v2/forms`)

  /**
   * fetch menus
   */
  const fetchedMenus: any = await axios.get(`${url}/wp-json/menus/v1/menus/`)

  const menus: any[] = []
  const fetchMenuData = async () => {
    await asyncForEach(fetchedMenus.data, async (menu: any) => {
      const result: any = await axios.get(
        `${url}/wp-json/menus/v1/menus/${menu.slug}`
      )
      const items: any[] = []
      result.data.items.forEach((item: any) => {
        const newUrl = item.url.replace(url, '').replace(/\/$/, '')
        items.push({
          title: item.title,
          url: newUrl
        })
      })
      menus.push({
        name: result.data.name,
        items
      })
    })
  }

  await fetchMenuData()

  /**
   * store fetched data
   */

  if (fetchedFrontpageEN) {
    data.frontpageEN = fetchedFrontpageEN.data
  }

  if (fetchedFrontpageNL) {
    data.frontpageNL = fetchedFrontpageNL.data
  }

  if (fetchedPages) {
    data.pages = fetchedPages.data
  }

  if (fetchedForms) {
    data.forms = fetchedForms.data
  }

  if (menus.length) {
    data.menus = menus
  }

  // get JWT bearer token
  let result: any

  try {
    result = await axios({
      method: 'post',
      url: `${url}/wp-json/jwt-auth/v1/token`,
      data: {
        username: process.env.USERNAME,
        password: process.env.PASSWORD
      }
    })
  } catch (error) {
    // console.log(Object.keys(error), error.message)
  }

  if (result && result.data.token) {
    data.bearerToken = result.data.token
  }

  return data
}

async function asyncForEach(array: any[], callback: Function) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

export default router
