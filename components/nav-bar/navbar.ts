import { Component, Vue, Watch, Ref } from 'vue-property-decorator'
import { Getter } from 'vuex-class'
import debounce from 'lodash/debounce'
import Logo from '~/components/Logo.vue'

@Component({
  name: 'Navbar',
  components: {
    Logo
  },
  computed: {
    menu() {
      return this.$store.getters[`menu${this.$i18n.locale.toUpperCase()}`]
    },
    pages() {
      return this.$store.getters[`pages${this.$i18n.locale.toUpperCase()}`]
    }
  }
})
class Navbar extends Vue {
  debounce: number = 10
  debouncedWindowResize: any = null
  toggledNav: boolean = false;

  @Getter isScrolling?: boolean
  @Getter scrollingOffsetReached?: boolean

  // created() {
  //   console.log('Navbar - locale:', this.$i18n.locale.toUpperCase())
  // }

  get isHomepage(): boolean {
    return !!this.$store.state.page.isHomepage
  }

  get showLogo(): boolean {
    return !!(this.isHomepage && this.scrollingOffsetReached) || !this.isHomepage
  }

  get showTitle(): boolean {
    return !!(this.isHomepage && this.scrollingOffsetReached) || !this.isHomepage
  }

  beforeMount() {
    this.debouncedWindowResize = debounce(this.windowResize, this.debounce)

    window.addEventListener('resize', this.debouncedWindowResize)
  }

  beforeDestroy() {
    window.removeEventListener('resize', this.debouncedWindowResize)
  }

  @Watch('$route', { immediate: true, deep: true })
  routeChangedHandler(val: string, oldVal: string) {
    this.toggledNav = false
  }

  windowResize(e: Event) {
    this.toggledNav = false
  }
}

export default Navbar
