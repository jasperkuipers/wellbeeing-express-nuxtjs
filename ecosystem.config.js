module.exports = {
  apps: [
    {
      name: 'wellbeeing',
      script: './node_modules/.bin/nuxt-ts',
      args: 'start',
      instances: 1,
      exec_mode: 'cluster',
      wait_ready: true,
      listen_timeout: 5000
    }
  ]
}
