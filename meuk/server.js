"use strict";
exports.__esModule = true;
var path_1 = require("path");
require('dotenv').config();
var http = require('http');
var https = require('https');
var _a = require('nuxt'), Nuxt = _a.Nuxt, Builder = _a.Builder;
var app = require('express')();
var fs = require('fs-extra');
var config = require('./nuxt.config.ts');
// create a server
var server;
var isProd = (process.env.NODE_ENV === 'production');
var port = process.env.PORT || 3000;
// prepare for http or https
if (process.env.NODE_ENV === 'production') {
    var pkey = fs.readFileSync(path_1["default"].resolve(__dirname, 'certificates/dekipenkorrel.nl.key'));
    var pcert = fs.readFileSync(path_1["default"].resolve(__dirname, 'certificates/dekipenkorrel.nl.crt'));
    var httpsOptions = {
        key: pkey,
        cert: pcert
    };
    server = https.createServer(httpsOptions, app);
}
else {
    server = http.createServer(app);
}
// we instantiate nuxt.js with the options
config.dev = !isProd;
var nuxt = new Nuxt(config);
// Render every route with Nuxt.js
app.use(nuxt.render);
function listen() {
    // listen the server
    // app.listen(port, '0.0.0.0');
    server.listen(port, '0.0.0.0');
    console.log("Server listening on localhost:" + port + ".");
}
// build only in dev mode with hot-reloading
if (config.dev) {
    new Builder(nuxt).build()
        .then(listen)["catch"](function (error) {
        console.error(error);
        process.exit(1);
    });
}
else {
    listen();
}
