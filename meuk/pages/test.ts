import { Component } from 'nuxt-property-decorator'
import PageMixin from '~/mixins/page'

@Component({
  name: 'TestPage'
})
class TestPage extends PageMixin {
  mounted() {
    console.log('TestPage mounted')
  }
}
export default TestPage
