import path from 'path'

require('dotenv').config()

const http = require('http')
const https = require('https')
const { Nuxt, Builder } = require('nuxt')
const app = require('express')()
const fs = require('fs-extra')
const config = require('./nuxt.config.ts')

// create a server
let server: any

const isProd = process.env.NODE_ENV === 'production'
const port = process.env.PORT || 3000

// prepare for http or https
if (process.env.NODE_ENV === 'production') {
  const pkey = fs.readFileSync(path.resolve(__dirname, `certificates/${process.env.CERTIFICATE_KEY}`))
  const pcert = fs.readFileSync(path.resolve(__dirname, `certificates/${process.env.CERTIFICATE_CRT}`))

  const httpsOptions = {
    key: pkey,
    cert: pcert
  }

  server = https.createServer(httpsOptions, app)
} else {
  server = http.createServer(app)
}

// we instantiate nuxt.js with the options
config.dev = !isProd
const nuxt = new Nuxt(config)

// Render every route with Nuxt.js
app.use(nuxt.render)

function listen() {
  // listen the server

  // app.listen(port, '0.0.0.0');
  server.listen(port, '0.0.0.0')

  console.log(`Server listening on localhost:${port}.`)
}

// build only in dev mode with hot-reloading
if (config.dev) {
  new Builder(nuxt).build()
    .then(listen)
    .catch((error: any) => {
      console.error(error)
      process.exit(1)
    })
} else {
  listen()
}
