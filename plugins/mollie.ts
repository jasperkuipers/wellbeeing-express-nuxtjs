import axios from 'axios'

export const createMollieCustomer = async (name: string, email: string): Promise<string> => {
  const result: any = await axios.post('/api/mollie/create-customer', { name, email })
  const { data } = result
  const { id } = data

  // console.log('created mollie customer with ID', id)

  return id
}

export const getMollieCustomer = async (id: string): Promise<string> => {
  const result: any = await axios.post('/api/mollie/get-customer', { id })
  const { data } = result
  const { email } = data

  // console.log('get mollie customer with email', email)

  return email
}

export const createMolliePayment = async (id: string, language: string, type: string): Promise<any> => {
  const result: any = await axios.post('/api/mollie/create-payment', { id, language, type })
  const { data } = result

  // console.log('create mollie payment', data)

  return data
}

export const createMolliePaymentWithMandate = async (id: string): Promise<any> => {
  const result: any = await axios.post('/api/mollie/create-payment-with-mandate', { id })
  const { data } = result

  // console.log('create mollie payment with mandate', data)

  return data
}

export const createMollieSubscription = async (id: string, amount: string): Promise<any> => {
  const result: any = await axios.post('/api/mollie/create-subscription', { id, amount })
  const { data } = result

  // console.log('create mollie subscription', data)

  return data
}
