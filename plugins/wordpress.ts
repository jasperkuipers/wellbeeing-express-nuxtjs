import axios from 'axios'

interface PayloadCustomer {
  email: string
  id: string
  language: string
  name: string
  startDate: string
  type: string
}

const wordpressUrl = 'https://wp.wellbeeing.org'

export const createWordPressCustomer = async (bearerToken: string, payload: PayloadCustomer) => {
  const { email } = payload
  const { id } = payload
  const { language } = payload
  const { name } = payload
  const { startDate } = payload
  const { type } = payload

  let result: any

  try {
    result = await axios({
      method: 'post',
      url: `${wordpressUrl}/wp-json/api/v1/create-customer`,
      headers: {
        Authorization: `Bearer ${bearerToken}`
      },
      data: {
        email,
        id,
        language,
        name,
        start_date: startDate,
        type
      }
    })
  } catch (error) {
    // console.log(Object.keys(error), error.message)
  }

  if (result && result.data) {
    // console.log('wordpress customer created', result.data)
  }
}

export const getWordPressCustomerById = async (bearerToken: string, id: string) => {
  let result: any

  try {
    result = await axios({
      method: 'post',
      url: `${wordpressUrl}/wp-json/api/v1/get-customer-by-id`,
      headers: {
        Authorization: `Bearer ${bearerToken}`
      },
      data: {
        id
      }
    })
  } catch (error) {
    // console.log(Object.keys(error), error.message)
  }

  if (result && result.data) {
    return result.data
  }
}
