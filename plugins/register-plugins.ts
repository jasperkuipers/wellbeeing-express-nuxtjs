import Vue from 'vue'
import VueScrollTo from 'vue-scrollto'
import Vuelidate from 'vuelidate'
import { Component } from 'vue-property-decorator'

Vue.use(VueScrollTo)
Vue.use(Vuelidate)

Component.registerHooks(['validations'])
