import { Form, Person, Page } from '~/types'

export interface RootState {
  bearerToken: null | string
  forms: Form[]
  homepageEN: null | any
  homepageNL: null | any
  isScrolling: boolean
  locales: string[]
  locale: string
  menus: any[]
  page: null | Page
  pages: any[]
  people: Person[]
  scrollingOffsetReached: boolean
  wordpressUrl: string
}
