export * from './state'

export interface Form {
  acf?: null | {
    datesEn: any[];
    datesNL: any[];
  };
  id: number;
  slug: string;
  status: string;
  title: string;
}

export interface Page {
  id?: number
  isHomepage?: boolean
  title: string
  excerpt?: null | string
  hasMollieForm: boolean
  hasSubscriptionForm: boolean
  content: string
  masthead?: null | string
  mollieFormType?: null | string
  relatedPages?: null | any[]
  subscriptionFormType?: null | string
  translations?: null | {
    locale: string
    id: string | number
    slug: string
  }
  seo?: null | {
    title: null | string
    meta: null | string
  }
}

export interface Person {
  id: number
  firstName: string
  lastName: string
  contact: Contact
  gender: string
  ipAddress: string
  avatar: string
  address: Address
}

export interface Contact {
  email: string
  phone: string
}

export interface Address {
  city: string
  country: string
  postalCode: string
  state: string
  street: string
}

export interface Item {
  id: number
  title: string
}
