export default function () {
  this.nuxt.hook('generate:done', (context) => {
    // add any route you don't want in your sitemap
    // potentially get this from an .env file.
    const routesToExclude = ['/en', '/en/', '/login', '/api', '/courses/thank-you', '/nl/cursussen/bedankt']
    const allRoutes = Array.from(context.generatedRoutes)
    const routes = allRoutes.filter((route) => {
      const result = routesToExclude.filter((routeToExclude) => {
        return route.includes(routeToExclude)
      })

      return !result.length
    })

    this.nuxt.options.sitemap.routes = [...routes]
  })
}
