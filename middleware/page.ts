import { Page } from '~/types'

const URL = 'wp.wellbeeing.org'

/**
 * replace/update WP content strings
 */
function replaceContentString(content: string): string {
  let string: string
  let replace: string
  let regex: RegExp

  // URL's
  content = content.replace(new RegExp(URL, 'g'), '')

  // buttons
  string = 'wp-block-button__link'
  replace = 'btn btn-secondary'
  regex = new RegExp(string, 'g')

  content = content.replace(regex, replace)

  string = 'wp-block-button'
  replace = 'btn-wrapper'
  regex = new RegExp(string, 'g')

  content = content.replace(regex, replace)

  // videos
  string = 'wp-block-embed-youtube'
  replace = 'video'
  regex = new RegExp(string, 'g')

  content = content.replace(regex, replace)

  // anchors
  const anchors = content.match(/<a.*class=.btn btn-secondary.*.*?<\/a>/g)

  if (anchors?.length) {
    anchors.map((anchor) => {
      let svg: string

      if (anchor.match(/href="#/g)) {
        svg = '<svg width="1em" height="1em" viewBox="0 0 20 20" focusable="false" role="img" alt="icon" xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi-chevron-down b-icon bi"><g><path fill-rule="evenodd" d="M3.646 6.646a.5.5 0 01.708 0L10 12.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"></path></g></svg>'
      } else {
        svg = '<svg viewBox="0 0 16 16" width="1em" height="1em" focusable="false" role="img" aria-label="chevron right" xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi-chevron-right b-icon bi"><g><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></g></svg>'
      }

      const innerText = anchor.match(/<a [^>]+>([^<]+)<\/a>/)[1]

      string = innerText
      replace = `${innerText} ${svg}`
      regex = new RegExp(string, 'g')

      content = content.replace(regex, replace)
    })
  }

  // images
  string = '/wp-content/uploads/'
  replace = `${URL}${string}`
  regex = new RegExp(string, 'g')

  content = content.replace(regex, replace)

  string = 'wp-block-image'
  replace = 'image-wrapper d-flex justify-content-center'
  regex = new RegExp(string, 'g')

  content = content.replace(regex, replace)

  return content
}

export default function (context: any) {
  const { isHMR, app, store, route, params, error, redirect, res } = context

  // if middleware is called from hot module replacement, ignore it
  if (isHMR) {
    return
  }

  let currentLanguage = 'en'

  if (route.params.lang === 'nl') {
    currentLanguage = 'nl'
  }

  let slug: string = ''

  if (route.name === 'login') {
    // DO NOTHING
  } else if (route.name === 'lang-type-bedankt' || route.name === 'lang-type-thank-you') {
    // DO NOTHING
  } else if (
    !params.lang && (!params.pathMatch ||
    params.pathMatch === '/' ||
    params.pathMatch === 'nl')
  ) {
    // initial route '/' or '/nl'
    slug = store.getters[`homepage${store.state.locale.toUpperCase()}`]
      ? store.getters[`homepage${store.state.locale.toUpperCase()}`].slug
      : ''
  } else if (
    !params.pathMatch &&
    params.lang &&
    params.lang.includes(store.state.locale)
  ) {
    // route '/en/' or '/nl/'
    slug = store.getters[`homepage${store.state.locale.toUpperCase()}`]
      ? store.getters[`homepage${store.state.locale.toUpperCase()}`].slug
      : ''
  } else if (!params.pathMatch) {
    // route '/:slug'
    slug = params.lang
  } else {
    // route '/:slug'
    slug = params.pathMatch
  }

  // console.log('page - params', params)
  // console.log('page - slug', slug)

  const pageData = store.getters.getPageBySlug(slug.replace(/\/$/, '')) // slug with removed trailing slash

  let page: Page

  // console.log(route.name)

  if (route.name === 'login') {
    page = {
      title: 'Login',
      excerpt: '',
      content: '...',
      hasMollieForm: false,
      hasSubscriptionForm: false,
      isHomepage: undefined,
      masthead: undefined,
      relatedPages: undefined,
      subscriptionFormType: undefined,
      seo: undefined,
      translations: undefined
    }
  } else if (route.name === 'lang-type-bedankt' || route.name === 'lang-type-thank-you') {
    page = {
      title: 'Result',
      excerpt: '',
      content: '...',
      hasMollieForm: false,
      hasSubscriptionForm: false,
      isHomepage: undefined,
      masthead: undefined,
      relatedPages: undefined,
      subscriptionFormType: undefined,
      seo: undefined,
      translations: undefined
    }
  } else if (pageData) {
    page = {
      id: pageData.id,
      title: pageData.title.rendered,
      content: replaceContentString(pageData.content.rendered),
      excerpt: pageData.excerpt.rendered,
      hasMollieForm: false,
      hasSubscriptionForm: false
    }

    // add WP advanced custom fields data
    if (pageData.acf) {
      if (pageData.acf.masthead) {
        page.masthead = pageData.acf.masthead
      }

      if (pageData.acf.subscription_form && !pageData.acf.subscription_form.includes('select')) {
        page.hasSubscriptionForm = true
        page.subscriptionFormType = pageData.acf.subscription_form
      }

      if (pageData.acf.mollie_form && !pageData.acf.mollie_form.includes('select')) {
        page.hasMollieForm = true
        page.mollieFormType = pageData.acf.mollie_form
      }

      // specifically for WP front-pages
      if (pageData.acf.related_pages && pageData.acf.related_pages.length) {
        page.relatedPages = pageData.acf.related_pages
      }
    }

    if (pageData.isHomepage) {
      page.isHomepage = pageData.isHomepage
    }

    if (pageData.yoast_title && pageData.yoast_meta) {
      page.seo = {
        title: pageData.yoast_title,
        meta: pageData.yoast_meta
      }
    }

    if (pageData.translations) {
      page.translations = pageData.translations
    }
  } else {
    res.statusCode = 404

    let currentLanguage = 'en'

    if (route.params.lang === 'nl') {
      currentLanguage = 'nl'
    }

    let content: string
    let title: string

    if (currentLanguage === 'nl') {
      content = '<p>Deze pagina bestaat niet.</p><a href="/" title="Ga naar onze homepage">Ga naar onze homepage</a>'
      title = 'Helaas'
    } else {
      content = '<p>This page doesn\'t exist.</p><a href="/" title="Go to our homepage">Go to our homepage</a>'
      title = 'Too bad'
    }

    page = {
      title,
      excerpt: '',
      content,
      hasMollieForm: false,
      hasSubscriptionForm: false,
      isHomepage: undefined,
      masthead: undefined,
      relatedPages: undefined,
      subscriptionFormType: undefined,
      seo: undefined,
      translations: undefined
    }
  }

  store.commit('SET_PAGE', page)
}
