import { MutationTree, ActionTree, GetterTree } from 'vuex'
import axios from 'axios'
// import * as data from './data'
import { Form, RootState, Person, Page } from '~/types'

let url: string

if (process.env.NODE_ENV === 'production') {
  url = 'https://wellbeeing.org'
} else {
  url = 'http://localhost:3000'
}

const wordpressUrl = 'https://wp.wellbeeing.org'

interface PayloadEmail {
    name: string;
    email: string;
    language: string;
    paymentLink: string;
    startDate: string;
    type: string;
}

export const state = (): RootState => ({
  bearerToken: null,
  forms: [],
  homepageEN: null,
  homepageNL: null,
  isScrolling: false,
  locales: ['en', 'nl'],
  locale: 'en',
  menus: [],
  page: null,
  pages: [],
  people: [],
  scrollingOffsetReached: false,
  wordpressUrl
})

function setTransations(page: any, pages: any): any {
  if (
    page.polylang_translations &&
    page.polylang_translations.length > 1
  ) {
    const translatedPage = page.polylang_translations.filter(
      (translation: any) => {
        return translation.locale !== page.polylang_current_lang
      }
    )[0]
    if (translatedPage) {
      const translatedPageData = pages.filter(
        (fetchedPage: any) => {
          return translatedPage.id === fetchedPage.id
        }
      )[0]
      const slug: string =
        (translatedPageData.polylang_current_lang === 'nl_NL'
          ? '/nl'
          : '/en') +
        (translatedPageData.slug.indexOf('homepage') === -1
          ? '/' + translatedPageData.slug
          : '')

      page.translations = {
        locale: translatedPageData.polylang_current_lang,
        id: translatedPageData.id,
        slug: slug
      }
    }
  }

  return page
}

export const getters: GetterTree<RootState, RootState> = {
  bearerToken: (state: RootState) => {
    return state.bearerToken
  },
  forms: (state: RootState): Form[] => {
    return state.forms
  },
  getFormBySlug: (state: RootState) => (slug: string) => {
    if (!state.forms) {
      return
    }

    return state.forms.find(form => form.slug === slug)
  },
  getPageById: (state: RootState) => (id: number) => {
    if (state.pages) {
      return state.pages.find(page => page.id === id)
    }
  },
  getPageBySlug: (state: RootState) => (slug: string) => {
    if (state.pages) {
      if (slug === 'homepage-en') {
        return state.homepageEN
      }

      if (slug === 'homepage-nl') {
        return state.homepageNL
      }

      return state.pages.find(page => page.slug === slug)
    }
  },
  getPageSlugById: (state: RootState) => (id: number) => {
    if (state.pages) {
      const page = state.pages.find(page => page.id === id)

      if (!page) {
        return
      }

      return page.slug
    }
  },
  homepageEN: (state: RootState) => {
    return state.homepageEN
  },
  homepageNL: (state: RootState) => {
    return state.homepageNL
  },
  isScrolling: (state: RootState): boolean => {
    return state.isScrolling
  },
  menuEN: (state: RootState): object => {
    return state.menus.filter(
      menu => menu.name.toLowerCase().indexOf('(en)') !== -1
    )[0]
  },
  menuNL: (state: RootState): object => {
    return state.menus.filter(
      menu => menu.name.toLowerCase().indexOf('(nl)') !== -1
    )[0]
  },
  page: (state: RootState) => {
    return state.page
  },
  pages: (state: RootState): object => {
    return state.pages
  },
  pagesEN: (state: RootState): object => {
    return state.pages.filter(page => page.polylang_current_lang === 'en_GB')
  },
  pagesNL: (state: RootState): object => {
    return state.pages.filter(page => page.polylang_current_lang !== 'en_GB')
  },
  pageContent: (state: RootState) => {
    return state.page ? state.page.content : ''
  },
  pageExcerpt: (state: RootState) => {
    return state.page ? state.page.excerpt : ''
  },
  pageHasMollieForm: (state: RootState) => {
    return state.page ? state.page.hasMollieForm : undefined
  },
  pageHasSubscriptionForm: (state: RootState) => {
    return state.page ? state.page.hasSubscriptionForm : undefined
  },
  pageIsHomepage: (state: RootState) => {
    return state.page ? state.page.isHomepage : undefined
  },
  pageMasthead: (state: RootState) => {
    return state.page ? state.page.masthead : undefined
  },
  pageMollieFormType: (state: RootState) => {
    return state.page && state.page.hasMollieForm ? state.page.mollieFormType : undefined
  },
  pageRelatedPages: (state: RootState) => {
    return state.page && state.page.relatedPages ? state.page.relatedPages : undefined
  },
  pageSeo: (state: RootState) => {
    return state.page ? state.page.seo : undefined
  },
  pageSubscriptionFormType: (state: RootState) => {
    return state.page && state.page.hasSubscriptionForm ? state.page.subscriptionFormType : undefined
  },
  pageTitle: (state: RootState) => {
    return state.page ? state.page.title : ''
  },
  scrollingOffsetReached: (state: RootState): boolean => {
    return state.scrollingOffsetReached
  },
  wordpressUrl: (state: RootState): string => {
    return state.wordpressUrl
  }
}

export const mutations: MutationTree<RootState> = {
  SET_BEARER_TOKEN(state: RootState, bearerToken: string): void {
    state.bearerToken = bearerToken
  },
  SET_FORMS(state: RootState, forms: Form[]): void {
    state.forms = forms
  },
  SET_HOMEPAGE_EN(state: RootState, homepageEN: any): void {
    state.homepageEN = homepageEN
  },
  SET_HOMEPAGE_NL(state: RootState, homepageNL: any): void {
    state.homepageNL = homepageNL
  },
  SET_IS_SCROLLING(state: RootState, isScrolling: boolean): void {
    state.isScrolling = isScrolling
  },
  SET_LANG(state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  },
  SET_MENUS(state: RootState, menus: any[]): void {
    state.menus = menus
  },
  SET_PEOPLE(state: RootState, people: Person[]): void {
    state.people = people
  },
  SET_PAGE(state: RootState, page: Page): void {
    state.page = page
  },
  SET_PAGES(state: RootState, pages: any[]): void {
    state.pages = pages
  },
  SET_SCROLLING_OFFSET_REACHED(state: RootState, scrollingOffsetReached: boolean): void {
    state.scrollingOffsetReached = scrollingOffsetReached
  }
}

export const actions: ActionTree<RootState, RootState> = {
  async nuxtServerInit({ commit, dispatch, getters }, context) {
    // console.log('context.app', context.app)
    // console.log('context.route.path', context.route.path)

    const result: any = await this.$axios.get(`${url}/api/wordpress`)

    if (result && result.data) {
      // console.log(result.data.test)

      /**
       * fetched WP homepage data
       */
      let homepageENData = result.data.frontpageEN
      let homepageNLData = result.data.frontpageNL

      // set translations
      if (homepageENData && homepageNLData) {
        const pages = [homepageENData, homepageNLData]

        homepageENData = setTransations(homepageENData, pages)
        homepageENData.isHomepage = true

        homepageNLData = setTransations(homepageNLData, pages)
        homepageNLData.isHomepage = true
      }

      if (homepageENData) {
        commit('SET_HOMEPAGE_EN', homepageENData)
      }

      if (homepageNLData) {
        commit('SET_HOMEPAGE_NL', homepageNLData)
      }
      /**
       * fetched WP pages data
       */
      const pagesData = result.data.pages

      if (pagesData) {
        const pages: any[] = []

        pagesData.forEach((page: any) => {
          // set translations
          setTransations(page, pagesData)

          pages.push(page)
        })

        commit('SET_PAGES', pages)
      }

      /**
       * fetched WP forms data
       */
      const formsData = result.data.forms

      if (formsData) {
        commit('SET_FORMS', formsData)
      }

      /**
       * fetched WP menus data
       */
      const menusData = result.data.menus

      if (menusData) {
        commit('SET_MENUS', menusData)
      }

      /**
       * authorization: jwt bearer token
       */
      const tokenData = result.data.bearerToken

      if (tokenData) {
        commit('SET_BEARER_TOKEN', tokenData)
      }
    }
  },
  updatePage({ commit, getters }, page: Page) {
    const currentPage: Page = getters.getPageById(page.id)
    const updatedPage: Page = { ...currentPage }

    const pages = getters.pages.filter((page: Page) => {
      return page.id !== currentPage.id
    })

    pages.push(updatedPage)

    commit('SET_PAGES', pages)
  },
  async sendEmail({ getters }, payload: PayloadEmail) {
    // console.log(`current bearer token: ${getters.bearerToken}`)

    let result: any

    try {
      result = await axios({
        method: 'post',
        url: `${wordpressUrl}/wp-json/api/v1/send-mail`,
        headers: {
          Authorization: `Bearer ${getters.bearerToken}`
        },
        data: {
          name: payload.name,
          email: payload.email,
          language: payload.language,
          payment_link: payload.paymentLink ? payload.paymentLink : null,
          start_date: payload.startDate ? payload.startDate : null,
          type: payload.type
        }
      })
    } catch (error) {
      // console.log('error', Object.keys(error), error.message)
    }

    if (result && result.data) {
      // console.log(`send email: ${result.data}`)
    }

    return result
  }
}
