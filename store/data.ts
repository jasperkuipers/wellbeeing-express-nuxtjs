import { ActionTree, MutationTree, GetterTree, ActionContext } from 'vuex'

export const name = 'data'
export const namespaced = true

export interface State {
  array: Array<number>
}

export const state = (): State => ({
  array: []
})

export const getters: GetterTree<State, State> = {
  array: (state: State) => {
    return state.array
  }
}

export const mutations: MutationTree<State> = {
  mutateArray(state, data: Array<number>) {
    state.array = data
  }
}

export const actions: ActionTree<State, State> = {
  setArray({ commit }, array: Array<number>) {
    commit('mutateArray', array)
  }
}
